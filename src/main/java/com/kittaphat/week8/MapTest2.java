package com.kittaphat.week8;

public class MapTest2 {
    public static void main(String[] args) {
        Map2 map2 = new Map2("Map2", 10, 10);
        map2.print();

        for(int y=Map2.Y_MIN; y<=Map2.Y_MAX; y++) {
            for(int x=Map2.X_MIN; x<=Map2.X_MAX; x++) {
                if(map2.getX() != x && map2.getY() != y) {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
    
}
