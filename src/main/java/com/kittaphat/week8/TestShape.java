package com.kittaphat.week8;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        System.out.print("Rect1Area = ");
        System.out.print(rect1.getRect());
        System.out.println();
        System.out.print("Rect1Perimeter = ");
        System.out.print(rect1.getRectPerimeter());
        System.out.println();

        Rectangle rect2 = new Rectangle(5, 3);
        System.out.print("Rect2Area = ");
        System.out.print(rect2.getRect());
        System.out.println();
        System.out.print("Rect2Perimeter = ");
        System.out.print(rect2.getRectPerimeter());
        System.out.println();

        Triangle triangle1 = new Triangle(5, 5, 6);
        System.out.print("triangleArea = ");
        System.out.print(triangle1.getTriangle());
        System.out.println();
        System.out.print("trianglePerimeter = ");
        System.out.print(triangle1.getTrianglePerimeter());
        System.out.println();

        Circle circle1 = new Circle(1);
        System.out.print("CircleArea = ");
        System.out.print(circle1.getCircle());
        System.out.println();
        System.out.print("CirclePerimeter = ");
        System.out.print(circle1.getCirclePerimeter());
        System.out.println();

        Circle circle2 = new Circle(2);
        System.out.print("CircleArea = ");
        System.out.print(circle2.getCircle());
        System.out.println();
        System.out.print("CirclePerimeter = ");
        System.out.print(circle2.getCirclePerimeter());
        System.out.println();
    }
}
