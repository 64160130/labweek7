package com.kittaphat.week8;

public class MapTest1 {
    public static void main(String[] args) {
        Map1 map1 = new Map1("Map1", 5, 5);
        map1.print();

        for(int y=Map1.Y_MIN; y<=Map1.Y_MAX; y++) {
            for(int x=Map1.X_MIN; x<=Map1.X_MAX; x++) {
                if(map1.getX() != x && map1.getY() != y) {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }

    
}
