package com.kittaphat.week8;

public class Triangle {
    private int a;
    private int b;
    private int c;
    
    public Triangle(int a,int b,int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public double getTriangle(){
        double s = (a+b+c)/2;
        return Math.sqrt(s*((s-a)*(s-b)*(s-c)));
    }
    public double getTrianglePerimeter(){
        return a+b+c;
    }

}
