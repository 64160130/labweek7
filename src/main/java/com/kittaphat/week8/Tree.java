package com.kittaphat.week8;

public class Tree {
    private int x;
    private int y;

    public Tree(int x,int y){
        this.x = x;
        this.y = y;
    }
    public int TreePositionX(){
        return x;
    }
    public int TreePositionY(){
        return y;
    }
}
